const path = require('path');
const webpack = require('webpack');

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');


// `env` is an object containing values sent from the command line
// --env.VALUE will add a key of `VALUE: true`
// --env.VALUE=hello will add a key of 'VALUE: 'hello'` (set values will always be strings)
function webpackConfig (env) {
  return {
    // context is the absolute path for resolving entry files, i.e., it means you can avoid
    // including './assets' in each entry point's path
    // context: path.resolve(__dirname, 'assets'),
    mode: env && env.NODE_ENV,
    devServer: {
      contentBase: path.resolve(__dirname, "./build"),
      historyApiFallback: true,
      port: 3000,
      stats: 'errors-only',
      open: true
    },
    entry: {
      app: './app/index.jsx',
      styles: './app/styles/main.scss',
    },
    output: {
      path: path.resolve(__dirname, './build'),
      filename: 'js/[name].js', // '[name]' will be replaced with the key in the `entry` object
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: ['babel-loader'],
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path][name].[ext]',
                outputPath: '/'
              }
            }
          ]
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: { url: false }
              }, {
                loader: 'sass-loader'
              }
            ]
          })
        }
      ]
    },
    plugins: getPlugins(env),
    resolve: {
      // `resolve.extensions` allows you to import files of these extensions without adding
      // the extension to the name, e.g. `import {app} from './index'`
      extensions: ['.jsx', '.js', '.css'],
      // `resolve.modules` allows you to leave off the included paths when you're importing files
      modules: [
        'node_modules',
        path.resolve(__dirname, 'js'),
      ],
    },
    devtool: getSourcemapConfig(env),
    optimization: { /* instead of "new webpack.optimize.CommonsChunkPlugin" */
      splitChunks: {
        chunks: 'all'
      }
    },
    performance: {
      maxAssetSize: 1000000,
      maxEntrypointSize: 1000000
    }
  };
}

function getPlugins (env) {
  const NODE_ENV = env && env.NODE_ENV;
  const envPlugin = new webpack.EnvironmentPlugin({
    NODE_ENV: NODE_ENV || 'development', // use 'development' as a default unless env.NODE_ENV is defined
  });

  const plugins = [
    new CopyWebpackPlugin([{from: './public'}]),
    new ExtractTextPlugin('css/app.css'),
    // this first CommonsChunkPlugin will pull out all code that is used in at least 2 places and
    // put it all into a new file, "js/vendor.js"
    // new webpack.optimize.CommonsChunkPlugin({ /* deprecated in webpack 4 */
    //   name: 'vendor',
    //   filename: 'js/vendor.js',
    //   minChunks: 2,
    // }),
    // this next CommonsChunkPlugin will pull out from JUST "js/vendor.js" any code from
    // "assets/js", which is where we keep our own code, and put it into a new file,
    // "js/common.js". that leaves the vendor bundle with just vendor code, so it should only change
    // when more common vendor code is added or dependencies get updated and the existing vendor
    // code changes
    // new webpack.optimize.CommonsChunkPlugin({ /* deprecated in webpack 4 */
    //   name: 'common',
    //   filename: 'js/common.js',
    //   minChunks: function isCommonFirstPartyCode (module) {
    //     return /\/js/.test(module.context);
    //   }
    // }),
    envPlugin,
    new HtmlWebpackPlugin({
      template: './public/index.html'
    })
  ];

  if (env && env.ANALYZE) {
    // uglify the bundle so the analyzer shows what it will look like in production
    plugins.push(new BundleAnalyzerPlugin(), new webpack.optimize.UglifyJsPlugin());
  }

  return plugins;
}

function getSourcemapConfig (env) {
  const NODE_ENV = env && env.NODE_ENV;

  return NODE_ENV === 'production' ? false : 'eval-source-map';
}

module.exports = webpackConfig;
