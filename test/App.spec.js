import React from 'react';
import expect from 'expect';

import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

const App = require('../app/App').default;
const Routes = require('../app/Routes').default;

describe('Testing App Component.', () => {

  it('should render component without errors', () => {
    const wrapper = shallow(<App />);

    const selector = '.main-info';
    expect(wrapper.find(selector).exists()).toBe(true);
    expect(wrapper.find(selector).childAt(0).contains(<Routes />)).toBe(true);
    expect(wrapper.find(selector).childAt(1).exists()).toBe(false);

    wrapper.unmount();
  });
});
