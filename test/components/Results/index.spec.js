import React from 'react';
import expect from 'expect';

import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

const Results = require('../../../app/components/Results').default;

describe('Testing Results/index.jsx Component.', () => {

  it('should render component without errors', () => {
    const wrapper = shallow(<Results />);

    const nestedArr = wrapper.children();
    expect(nestedArr.exists()).toBe(true);
    expect(nestedArr.length).toBe(3);

    expect(nestedArr.at(0).text()).toBe('Results Page');
    expect(nestedArr.at(1).name()).toBe('br');
    expect(nestedArr.at(2).name()).toBe('Link');

    expect(nestedArr.at(2).props().to).toBe('/dashboard');
    expect(nestedArr.at(2).props().children).toBe('Back to Dashboard');

    wrapper.unmount();
  });
});
