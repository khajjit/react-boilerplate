import React from 'react';
import expect from 'expect';

import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

const Payments = require('../../../app/components/Payments').default;

describe('Testing Payments/index.jsx Component.', () => {

  it('should render component without errors', () => {
    const wrapper = shallow(<Payments />);

    const nestedArr = wrapper.children();
    expect(nestedArr.exists()).toBe(true);
    expect(nestedArr.length).toBe(1);
    expect(nestedArr.text()).toBe('Payments Page');

    wrapper.unmount();
  });
});
