import React from 'react';
import expect from 'expect';

import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

const Settings = require('../../../app/components/Settings').default;

describe('Testing Settings/index.jsx Component.', () => {

  it('should render component without errors', () => {
    const wrapper = shallow(<Settings />);

    const nestedArr = wrapper.children();
    expect(wrapper.length).toBe(1);
    expect(wrapper.name()).toBe('Redirect');
    expect(wrapper.props().to).toBe('/dashboard');

    wrapper.unmount();
  });
});
