import React from 'react';
import expect from 'expect';

import { mountWrap } from '../../_misc/helpers';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

const Header = require('../../../app/components/Header').default.WrappedComponent;
const mapStateToProps = require('../../../app/components/Header').mapStateToProps;

describe('Testing Header/index.jsx Component.', () => {

  it('should render component without errors', () => {
    const wrapper = mountWrap(<Header />);

    const nestedArr = wrapper.children().children();
    expect(nestedArr.exists()).toBe(true);
    expect(nestedArr.length).toBe(5);

    expect(nestedArr.at(0).text()).toBe('STOREFRONT');
    expect(nestedArr.at(0).name()).toBe('h2');

    expect(nestedArr.at(1).name()).toBe('Link');
    expect(nestedArr.at(1).props().to).toBe('/dashboard');
    expect(nestedArr.at(1).props().children).toBe('Dashboard');

    expect(nestedArr.at(2).name()).toBe('Link');
    expect(nestedArr.at(2).props().to).toBe('/payments');
    expect(nestedArr.at(2).props().children).toBe('Payments');

    expect(nestedArr.at(3).name()).toBe('Link');
    expect(nestedArr.at(3).props().to).toBe('/settings');
    expect(nestedArr.at(3).props().children).toBe('Settings');

    expect(nestedArr.at(4).name()).toBe('hr');

    wrapper.unmount();
  });
});
