import React from 'react';
import expect from 'expect';

import { mountWrap } from '../../_misc/helpers';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { mapStateToProps } from '../../../app/components/Dashboard';
configure({ adapter: new Adapter() });

const Dashboard = require('../../../app/components/Dashboard').default.WrappedComponent;

describe('Testing Dashboard/index.jsx Component.', () => {

  it('should implement mapStateToProps', () => {
    const initialState = {
      mock: {
        appIsShowed: true
      }
    };
    expect(mapStateToProps(initialState).isAppInit).toEqual(true);
  });

  it('should render component without errors', () => {
    const wrapper = mountWrap(<Dashboard />);

    const nestedArr = wrapper.children().children();
    expect(nestedArr.exists()).toBe(true);

    expect(wrapper.children().props().children[0]).toBe('Dashboard Page');
    expect(nestedArr.at(0).name()).toBe('br');
    expect(nestedArr.at(1).name()).toBe('Link');

    expect(nestedArr.at(1).props().to).toBe('/results');
    expect(nestedArr.at(1).props().children).toBe('Go to results');

    wrapper.unmount();
  });
});
