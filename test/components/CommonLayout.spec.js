import React from 'react';
import expect from 'expect';

import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

const Header = require('../../app/components/Header').default;
const CommonLayout = require('../../app/components/CommonLayout').default;

describe('Testing CommonLayout Component.', () => {

  it('should render nothing for not authorized (with no parameters in function)', () => {
    const wrapper = shallow(CommonLayout());

    expect(wrapper.find('.wrapper').exists()).toBe(true);

    expect(wrapper.children().children().length).toBe(1);
    expect(wrapper.text()).toBe('Need to authorize');

    wrapper.unmount();
  });

  it('should render nothing for not authorized user', () => {
    const wrapper = shallow(CommonLayout(false));

    expect(wrapper.find('.wrapper').exists()).toBe(true);

    expect(wrapper.children().children().length).toBe(1);
    expect(wrapper.text()).toBe('Need to authorize');

    wrapper.unmount();
  });

  it('should render Header for authorized user', () => {
    const wrapper = shallow(CommonLayout(true));
    const nestedArr = wrapper.children().children();

    expect(wrapper.find('.wrapper').exists()).toBe(true);
    expect(nestedArr.length).toBe(2);

    expect(nestedArr.at(0).equals(<Header />)).toBe(true);

    wrapper.unmount();
  });

  it('should render Header and expected component for authorized user', () => {
    const ExpectedComponent = () => <div className="expected-component">Text of component</div>;
    const wrapper = shallow(CommonLayout(true, ExpectedComponent));
    const nestedArr = wrapper.children().children();

    expect(wrapper.find('.wrapper').exists()).toBe(true);
    expect(nestedArr.length).toBe(2);

    expect(nestedArr.at(0).equals(<Header />)).toBe(true);

    expect(nestedArr.at(1).children().name()).toBe('ExpectedComponent');
    expect(nestedArr.at(1).children().text()).toBe('<ExpectedComponent />');

    wrapper.unmount();
  });

  it('should render Header and expected component for authorized user', () => {
    const ExpectedComponent = () => <div className="expected-component">Text of component</div>;
    const wrapper = shallow(CommonLayout(true, ExpectedComponent, { mockProps: 'test-value' }));
    const nestedArr = wrapper.children().children();

    expect(wrapper.find('.wrapper').exists()).toBe(true);
    expect(nestedArr.length).toBe(2);

    expect(nestedArr.at(0).equals(<Header />)).toBe(true);

    expect(nestedArr.at(1).children().name()).toBe('ExpectedComponent');
    expect(nestedArr.at(1).children().text()).toBe('<ExpectedComponent />');

    expect(nestedArr.at(1).children().prop('mockProps')).toBe('test-value');

    wrapper.unmount();
  });
});
