import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import expect from 'expect';

import * as actionsCreators from '../../app/actions/mock';
import { MOCK as types } from '../../app/actions/constants';
import { mock as reducer } from '../../app/reducers/mock';
import rootReducer from '../../app/reducers/index';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('mock actions', () => {
  const localState = reducer();
  const globalState = rootReducer();

  describe('function startApp', () => {
    const action = { type: types.START_APP };
    const state = { appIsShowed: false };
    const expectedState = { appIsShowed: true };

    it('should work, reducer', () =>
      expect(reducer(state, action)).toEqual(expectedState)
    );

    it('should work, action', () => {
      const store = mockStore(state);
      store.dispatch(actionsCreators.startApp());
      expect(store.getActions()[0]).toEqual(action);
    });
  });
});
