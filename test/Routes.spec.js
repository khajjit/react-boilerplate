import React from 'react';
import expect from 'expect';

import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

const Routes = require('../app/Routes').default;
const Dashboard = require('../app/components/Dashboard').default;

describe('Testing Routes Component.', () => {

  it('should render router component without errors', () => {
    const wrapper = shallow(<Routes />);
    const nestedArr = wrapper.children().children();

    expect(nestedArr.length).toBe(5);

    expect(nestedArr.at(0).prop('path')).toBe('/dashboard');
    expect(nestedArr.at(1).prop('path')).toBe('/payments');
    expect(nestedArr.at(2).prop('path')).toBe('/settings');
    expect(nestedArr.at(3).prop('path')).toBe('/results');
    expect(nestedArr.at(4).prop('path')).toBe('/*');
    for (let i = 0; i < nestedArr.length; i++) {
      expect(nestedArr.at(i).name()).toBe('Route');
    }

    wrapper.unmount();
  });

  it('should render router component and nested component', () => {
    const wrapper = shallow(<Routes />);
    const ReceivedDashboard = wrapper
      .children() // ConnectedRouter
      .children().at(0) // Switch
      .prop('render')() // activate render on first Route
      .props // parent div in CommonLayout
      .children // next div
      .props.children[1] // second div in array after Header
      .props.children // nested expected component

    expect(ReceivedDashboard).toEqual(<Dashboard />);

    wrapper.unmount();
  });
});
