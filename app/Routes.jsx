// @flow
import React, { Component } from 'react'
import { ConnectedRouter } from 'connected-react-router'
import { Switch, Route } from 'react-router-dom'

import { history } from './utils'

import commonLayout from './components/CommonLayout'

import Dashboard from './components/Dashboard'
import Payments from './components/Payments'
import Settings from './components/Settings'

import Results from './components/Results'

class Routes extends Component<any> {
  protectedRender(component: Function) {
    return (args: Object = {}) =>
      commonLayout(true, component, args)
  }

  render() {
    return (
      <ConnectedRouter history={history}>
        <Switch>
          <Route exact path="/dashboard" render={this.protectedRender(Dashboard)} />
          <Route exact path="/payments" render={this.protectedRender(Payments)} />
          <Route exact path="/settings" render={this.protectedRender(Settings)} />

          <Route exact path="/results" render={this.protectedRender(Results)} />
          <Route path="/*" render={this.protectedRender(Dashboard)} />
        </Switch>
      </ConnectedRouter>
    )
  }
}

export default Routes
