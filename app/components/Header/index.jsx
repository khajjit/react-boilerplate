// @flow
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { StateType } from '../../types/states'

class Header extends Component<any> {
  render() {
    return (
      <div>
        <h2>
          STOREFRONT
        </h2>
        <Link to={`/dashboard`}>Dashboard</Link>
        <Link to={`/payments`}>Payments</Link>
        <Link to={`/settings`}>Settings</Link>
        <hr />
      </div>
    )
  }
}

const mapStateToProps = (state: StateType) => ({ mock: state.mock })

export default connect(mapStateToProps)(Header)
