// @flow
import React from 'react'

import Header from './Header/index.jsx'

const CommonLayout = (isAuthorized: boolean = false, Component: ?Function, props: Object = {}) => {
  return (
    <div className="wrapper">
      {
        isAuthorized ? (
          <div>
            <Header />
            <div>
              {Component && <Component {...props} />}
            </div>
          </div>
        ) : <div>Need to authorize</div>
      }
    </div>
  )
}

export default CommonLayout
