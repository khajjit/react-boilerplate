// @flow
import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'

class Settings extends Component<any> {
  render() {
    return (
      <Redirect to={`/dashboard`} />
    )
  }
}

export default Settings
