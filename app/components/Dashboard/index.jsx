// @flow
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { StateType } from '../../types/states'

class Dashboard extends Component<any> {
  render() {
    return (
      <div>
        Dashboard Page
        <br />
        <Link to={`/results`}>Go to results</Link>
      </div>
    )
  }
}

export const mapStateToProps = (state: StateType) => ({ isAppInit: state.mock.appIsShowed })

export default connect(mapStateToProps)(Dashboard)
