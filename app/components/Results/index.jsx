// @flow
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class Results extends Component<any> {
  render() {
    return (
      <div>
        Results Page
        <br />
        <Link to={`/dashboard`}>Back to Dashboard</Link>
      </div>
    )
  }
}

export default Results
