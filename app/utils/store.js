// @flow
import logger from 'redux-logger'
import thunkMiddleware from 'redux-thunk'
import { applyMiddleware, createStore, compose } from 'redux'
import { routerMiddleware, connectRouter } from 'connected-react-router'

import { history } from './history'
import reducer from '../reducers'

let middleware = []

if (process.env.NODE_ENV === 'development') {
  middleware = [
    routerMiddleware(history),
    thunkMiddleware,
    logger
  ]
} else {
  middleware = [
    routerMiddleware(history),
    thunkMiddleware
  ]
}

const store = createStore(
  connectRouter(history)(reducer),
  compose(
    applyMiddleware(
      ...middleware
    )
  )
)

export { store }
