// @flow
import 'babel-polyfill'
import 'whatwg-fetch'

import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import { store } from './utils'
import App from './App'

const main = document.getElementById('main')
if (main == null) {
  throw new Error('No main element!')
} else {
  render(
    <Provider store={store}>
      <App />
    </Provider>,
    main
  )
}
