// @flow
import { MOCK } from './constants'

export const startApp = () =>
  (dispatch: Function) =>
    dispatch({ type: MOCK.START_APP })
