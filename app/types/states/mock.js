// @flow

export type MockStateType = {|
  appIsShowed: boolean
|}
