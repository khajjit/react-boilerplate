import type { MockStateType } from './mock'

export type StateType = {|
  mock: MockStateType
|}
