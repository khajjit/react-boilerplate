// @flow
import { MOCK } from '../actions/constants'
import type { MockStateType } from '../types/states/mock'

const initialState: MockStateType = {
  appIsShowed: false
}

export function mock(
  state: MockStateType = initialState,
  action: Object = {}
): MockStateType {
  switch (action.type) {
    case MOCK.START_APP:
      return {
        ...state,
        appIsShowed: true
      }
    default:
      return state
  }
}
