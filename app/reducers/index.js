// @flow
import { combineReducers } from 'redux'

import { mock } from './mock'

const reducer = combineReducers({
  mock
})

export default reducer
