// @flow
import React, { Component } from 'react'

import Routes from './Routes'

class App extends Component<any> {
  render() {
    return (
      <div className="main-info">
        <Routes />
      </div>
    )
  }
}

export default App
